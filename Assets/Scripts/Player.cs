﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	public float moveSpeed = 1.5f;

	public Transform reticleTransform;
    Vector2 mousePos;
	public float maxReticleDistance = 4f;
	public float minReticleDistance = 1f;
	public float reticleMoveSpeed = .1f;

	public GameObject projectileYo;
	public GameObject emitterYo;
    public Transform reticleConstraint;
    public bool gameOver = false;

    GameManager gm;
    float currentTimer = 0f;
    float timeTillNextShot;
    Camera cam;

 
	// Start is called before the first frame update
	void Start()
    {
        gm = FindObjectOfType<GameManager>();
        timeTillNextShot = gm.timeTillNextShot;
        cam = FindObjectOfType<Camera>();
        reticleConstraint = GameObject.FindGameObjectWithTag("ReticleConstraint").transform;
    }

    // Update is called once per frame
    void Update()
    {
        currentTimer += Time.deltaTime;

        if (gameOver == true)
        {
            return;
        }
		//GetInputKeyboard();
        GetInputMouse();
    }

	//void GetInputKeyboard()
	//{   
 //       // buffer inputs
	//	var hor = Input.GetAxisRaw("Horizontal");
	//	var vert = Input.GetAxisRaw("Vertical");


	//	// Check for horizontal movement
	//	if (hor > 0)
	//		transform.Rotate(0, 0, -moveSpeed);
	//	else if (hor < 0)
	//		transform.Rotate(0, 0, moveSpeed);

	//	// Check for vertical movement (reticle)
	//	if (vert > 0 && reticleTransform.localPosition.y < maxReticleDistance)
	//		reticleTransform.localPosition = reticleTransform.localPosition + Vector3.up * reticleMoveSpeed;
	//	else if (vert < 0 && reticleTransform.localPosition.y > minReticleDistance)
	//		reticleTransform.localPosition = reticleTransform.localPosition - Vector3.up * reticleMoveSpeed;

	//	if (Input.GetButtonDown("Jump"))
 //       {
 //           if (currentTimer >= timeTillNextShot)
 //           {
 //               ShootyMcShootShoot();
 //               currentTimer = 0;
 //           }
 //       }
	//}

    void GetInputMouse()
    {
        // Get the Screen positions of the object
        Vector2 positionOnScreen = Camera.main.WorldToViewportPoint(transform.position);

        // Get the Screen position of the mouse
        Vector3 mouseOnScreen = (Vector3)Camera.main.ScreenToViewportPoint(Input.mousePosition);

        // Get the angle between the points
        float angle = AngleBetweenTwoPoints(positionOnScreen, mouseOnScreen);

        float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
        {
            return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
        }

        transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, (angle + 90)));

        RaycastHit2D rayHit = Physics2D.GetRayIntersection(Camera.main.ScreenPointToRay(Input.mousePosition));
        //print(rayHit.point);
        float mouseDist = Vector2.Distance(rayHit.point, Vector2.zero);
        float reticleDist = Vector2.Distance(reticleTransform.position, Vector2.zero);
        float emitterDist = Vector2.Distance(emitterYo.transform.position, Vector2.zero);

        //print("emitterDist = " + emitterDist);
        //if (reticleDist > 6)
        //{
        //    print("reticleDist = " + reticleDist);
        //}
        
        if (mouseDist <= (emitterDist))
        {  
            reticleTransform.position = reticleConstraint.transform.position;
        }

        else
        {
            reticleTransform.position = rayHit.point;
        }
        

        if (Input.GetMouseButtonDown(0))
        {
            if (currentTimer >= timeTillNextShot)
            {
                ShootyMcShootShoot();
                currentTimer = 0;
            }
        }
    }


    void ShootyMcShootShoot()
	{
		Shooty shooty = Instantiate(projectileYo, emitterYo.transform.position, transform.rotation).GetComponent<Shooty>();
		shooty.InitializeDatShootyTho(10, 10, reticleTransform.position);
		gm.modifyScore(-10);
		gm.am.Play("PlayerShotFired");
	}
}

