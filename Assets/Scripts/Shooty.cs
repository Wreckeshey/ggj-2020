﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooty : MonoBehaviour
{
	float moveSpeed = 1f;
	float blastRadius = 10f;
	Vector3 explosionLocation;
	float explosionThreshold = 1f;
	//float lifeSpan = 3f;
	//float timer = 0f;

	public GameObject shootyExplodey;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		transform.position += transform.up * moveSpeed * Time.deltaTime;

		//timer += Time.deltaTime;
		//if (timer > lifeSpan)
		//	Destroy(this.gameObject);

		if (Vector3.Distance(explosionLocation, transform.position) < explosionThreshold)
			ExplodeBitch();

    }

	void ExplodeBitch()
	{
		// Instantiate the prefab or whatever
		Instantiate(shootyExplodey, transform.position, new Quaternion());

		//print("BOOOOOM!!");
		Destroy(this.gameObject);
	}

	/// <summary>
	/// Optional function to overide default projectile settings. 
	/// </summary>
	/// <param name="_moveSpeed"></param>
	/// <param name="_blastRadius"></param>
	public void InitializeDatShootyTho(float _moveSpeed, float _blastRadius, Vector3 _explosionLocation)
	{
		moveSpeed = _moveSpeed;
		blastRadius = _blastRadius;
		explosionLocation = _explosionLocation;
	}
}
