﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootyExplody : MonoBehaviour
{
    GameManager gm;
    public float explosionSize;
    public float explosionDurration;
    public float currentExplosionSize;
    public bool reachedMaxSize = false;
    public float startingExplosionSize;
    Vector3 maxExplosionSize;    

    // Timer
    public float currentTime = 0f;

    // Start is called before the first frame update
    void Start()
    {
        gm = FindObjectOfType<GameManager>();
        gm.am.Play("PlayerShotExplosion");
        explosionSize = gm.explosionSize;
        explosionDurration = gm.explosionDurration;
        startingExplosionSize = gm.startingExplosionSize;
        currentExplosionSize = startingExplosionSize;

        maxExplosionSize = new Vector3(explosionSize, explosionSize, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (reachedMaxSize == false)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, maxExplosionSize, currentTime/(explosionDurration / 2));
            //print("local scale = " + transform.localScale.x);
            if (transform.localScale.x >= (explosionSize - .01))
            {                
                reachedMaxSize = true;
            }
        }        

        else 
        {
            transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(startingExplosionSize, startingExplosionSize, 0), currentTime / (explosionDurration / 2 ));
        }

        if (transform.localScale.x <= (startingExplosionSize + .01)) 
        {
            Destroy(this.gameObject);
        }

        currentTime += Time.deltaTime;
    }
}
