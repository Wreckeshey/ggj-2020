﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{  
    public Vector2 target;
    public GameObject EnemyExplosionWithPlanet;
    public GameObject EnemyExplosionWithShootyExplody;
    float enemyMovementSpeedMin;
    float enemyMovementSpeedMax;
    float enemyMovementSpeed;
    public float targetAccuracy; //?
    GameManager gm;
    


    void Start()
    {
		gm = FindObjectOfType<GameManager>();
		ChooseTarget();
        enemyMovementSpeedMin = gm.enemyMovementSpeedMin;
        enemyMovementSpeedMax = gm.enemyMovementSpeedMax;

        enemyMovementSpeed = Random.Range(enemyMovementSpeedMin, enemyMovementSpeedMax);
        // choose target
        // set speed?         
    }
    void ChooseTarget()
    {
		// use world radius to create random target based off world center
		target = new Vector3(Random.Range(-gm.targetOffset, gm.targetOffset),Random.Range(-gm.targetOffset,gm.targetOffset));
    }

    void Update()
    {
        Movement();

        // destroys game object if misses target and doesn't get destroyed by player
      
    }

    void Movement()
    {
        float step = enemyMovementSpeed * Time.deltaTime;
        transform.position = Vector2.MoveTowards(transform.position, target, step);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {        
        if (collision.gameObject.tag == "ShootyExplody")
        {
            // Create tag for exlosion (player shot)            
            ShotDown();
        }
        
        else if (collision.gameObject.tag == "Planet")
        {
            // Call for damage to Planet
            gm.PlanetDamage();
            ExplodeOnPlanet();
        }

        else if (collision.gameObject.tag == "Player")
        {
            // Call for damage to Player
            ExplodeOnPlanet();
        }
        // check for collision with target and check collision with player
        // check for collision with player shot
    }

    public void ExplodeOnPlanet()
    {
        // destroy game object and spawn destroyed effect 
        Instantiate(EnemyExplosionWithPlanet, this.gameObject.transform.position, new Quaternion());
        // audio hook for explosion here
        gm.am.Play("PlanetTakeDamage");
        Destroy(this.gameObject);
    }

    public void ShotDown()
    {
		// gm.currentScore += 100;
		gm.modifyScore(100);
		// destroy game object and spawn destroyed effect 
        Instantiate(EnemyExplosionWithShootyExplody, this.gameObject.transform.position, new Quaternion());
        // audio hook for explosion here
        gm.am.Play("EnemyExplosion");
        Destroy(this.gameObject);
    }

}
