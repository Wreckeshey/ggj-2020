﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    public GameObject enemy;
    public float screenSize = 25f;
    MainCamera mainCamera;
    [Header("Screen Shake Variables")]
    public float cameraShakeDuration = .15f;
    public float cameraShakeMagnitude = .4f;

    // References or whatever
    [Header("Planet Size Variables")]
    GameObject planet;
	public float planetScale = 2.5f;
	public float targetOffset;
	public GameObject player;

    // Explosion size and durration
    [Header ("Shooty Explody Variables")]
    public float timeTillNextShot;
    public float explosionSize;
    public float explosionDurration;
    public float startingExplosionSize;

    // Planet Health
    [Header ("Planet Health")]
    public int maxPlanetHealth = 4;
    public int currentPlanetHealth;
    SpriteRenderer sr;
    Sprite[] sprites;
    int planetHealthSprite = 4;

    // Game is running
    bool gameOver = false;
    float currentSpawnTimer;
    float spawnTimer;
    [Header ("Spawn Timer")]
    public float minTimer = .5f;
    public float maxTimer = 3f;
    float timerDifficultyIncrease = 10f;
    float timerDifficultyCurrent = 0f;

    [Header("Enemy Movement")]
    public float enemyMovementSpeedMin = 5;
    public float enemyMovementSpeedMax = 15;

	[Header("Power Ups")]
	// this is going to be in the game manager
	public GameObject powerUpPrefab;
	public float traversePowerUpDistance = 70f;
	public float minPowerUpDistance = 7f;
	public float maxPowerUpDistance = 20f;



	// Scoreboard
	[HideInInspector]
    private int currentScore = 0;
	private int hiddenScore = 0;
    int highScore;
    TextMeshProUGUI score;
    TextMeshProUGUI gameOverScreen;
    [HideInInspector]
    public AudioManager am;
      

    void Start()
    {
        Cursor.visible = false;
        SetReferences();
        currentScore = 0;
        SetScore();

		// Scale planet and set player offset
        planet.transform.localScale = new Vector3(planetScale,planetScale,1);
		player.transform.Find("PlayerTank").transform.localPosition = new Vector3(0, planetScale * .55f, 0);
		targetOffset = planetScale * .4f; // the idea here is you want this to be about the same as the radius of the planet (roughly where the tank sits)

		currentPlanetHealth = maxPlanetHealth;
        SpawnTimer();        
        planetHealthSprite = 4;
        
        SetPlanetHealthSprite();
        player.GetComponent<Player>().gameOver = false;
        highScore = PlayerPrefs.GetInt("highScore");
        gameOverScreen.text = "";
        timerDifficultyCurrent = 0;
        enemyMovementSpeedMin = 4;
        enemyMovementSpeedMax = 7;
    }

	/// <summary>
	/// Positive and negative values affect visible score. Only positive values affect the hidden score.
	/// </summary>
	/// <param name="points"></param>
	public void modifyScore(int points)
	{
		currentScore += points;

		if (points > 0)
		{
			hiddenScore += points;
			if (hiddenScore % 1000 == 0)
				SpawnPowerUp();
		}
	}


	void SpawnPowerUp()
	{
		//print("spawn power up!");

		Vector2 dir = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
		float tangentDistance = Random.Range(minPowerUpDistance, maxPowerUpDistance);
		Vector2 tangentPoint = dir.normalized * tangentDistance;
		Vector2 startPoint = tangentPoint - (Vector2.Perpendicular(dir).normalized * traversePowerUpDistance / 2);

		PowerUp powerUp = Instantiate(powerUpPrefab, startPoint, Quaternion.identity).GetComponent<PowerUp>();
		powerUp.InitializePowerUp(dir);

	}

	void SetReferences()
    {
        planet = GameObject.FindGameObjectWithTag("Planet");
        player = GameObject.FindGameObjectWithTag("Player");
        score = GameObject.FindGameObjectWithTag("ScoreCount").GetComponent<TextMeshProUGUI>();
        gameOverScreen = GameObject.FindGameObjectWithTag("GameOver").GetComponent<TextMeshProUGUI>();
        mainCamera = FindObjectOfType<MainCamera>();
        sprites = Resources.LoadAll<Sprite>("planetSpriteSheet");
        sr = planet.GetComponent<SpriteRenderer>();
        am = FindObjectOfType<AudioManager>();
    }

    void Update()
    {
        SetScore();
        if (gameOver == false)
        {
            if(currentSpawnTimer >= spawnTimer)
            {   
                SpawnEnemy();            
            }
            DifficutlyIncreaseCheck();
        }

        // Restarts scene on Game Over
        else if (gameOver == true)
        {
            if(Input.GetKeyDown(KeyCode.Return))
            {
                am.Play("StartGame");
                Scene scene = SceneManager.GetActiveScene();
                SceneManager.LoadScene(scene.name);
            }
        }
        currentSpawnTimer += Time.deltaTime;
    }

    void DifficutlyIncreaseCheck()
    {
        timerDifficultyCurrent += Time.deltaTime;
        if (timerDifficultyCurrent >= timerDifficultyIncrease)
        {
            enemyMovementSpeedMin *= 0.90f;
            enemyMovementSpeedMax *= 0.90f;
            enemyMovementSpeedMin *= 1.1f;
            enemyMovementSpeedMax *= 1.1f;

            timerDifficultyCurrent = 0f;
        }
    }

    void SetScore()
    {
        score.text = ("<br>  Current Score = " + currentScore + "<br>  High Score = " + highScore);
    }
    
    void SpawnTimer()
    {
        spawnTimer = Random.Range(minTimer, maxTimer);
    }

    void SetPlanetHealthSprite()
    {
        sr.sprite = sprites[planetHealthSprite];
    }

    void SpawnEnemy()
    {   
        // this ensures the enemy spawns on the edge of screen
        float x = Random.Range(-screenSize, screenSize);
        float y = Random.Range(-screenSize, screenSize);
        if ((Mathf.Abs(x) + Mathf.Abs(y)) <= screenSize)
        {
            SpawnEnemy();
            return;
        }
        Vector2 spawnPoint = new Vector2(x, y);
        Instantiate(enemy, spawnPoint, Quaternion.identity);
        SpawnTimer();
        currentSpawnTimer = 0;
    }

    public void PlanetDamage()
    {
        // screen shake
        StartCoroutine(mainCamera.Shake(cameraShakeDuration, cameraShakeMagnitude));
        // Audio que for PlanetTakeDamage in the Enemy Script        
        // Take Planet Damagae
        currentPlanetHealth--;
        planetHealthSprite--;
        if (planetHealthSprite <= 0)
        {
            planetHealthSprite = 0;
        }
        SetPlanetHealthSprite();

        if (currentPlanetHealth <= 0)
        {
            GameOver();
        }
    }

    void GameOver()
    {
        gameOver = true;
        Enemy[] enemies = FindObjectsOfType<Enemy>();
        foreach (Enemy enemy in enemies)
        {
            enemy.ExplodeOnPlanet();
        }
        player.GetComponent<Player>().gameOver = true;
        if (highScore < currentScore)
        {
            highScore = currentScore;
            PlayerPrefs.SetInt("highScore", highScore);
        }

        gameOverScreen.text = "---GameOver---<br>Press Return to Play";
    }
}
