﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
	public float moveSpeed = 5f;
	public PowerUpType powerUpType;
	Vector2 dir;

	// Start is called before the first frame update
	void Start()
    {


		//transform.position = startPoint;
		//// transform.rotation = Quaternion.Euler(Vector2.Perpendicular(dir).normalized);
		//print("start: " + startPoint);
		//print("tangent: " + tangentPoint);
		//print("dir: " + dir);
		
    }

    // Update is called once per frame
    void Update()
    {
		transform.position += Vector3.Normalize(Vector2.Perpendicular(dir)) * moveSpeed * Time.deltaTime;
	}

	public void InitializePowerUp(Vector2 _dir)
	{
		dir = _dir;
	}

	void GivePowerUp()
	{

	}

	void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.tag == "ShootyExplody")
		{
			GivePowerUp();
			Destroy(this.gameObject);
		}
	}

	public enum PowerUpType
	{
		tripleShot, rapidFire
	}
}
